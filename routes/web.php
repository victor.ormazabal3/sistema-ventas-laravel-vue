<?php
use App\Articulo;

// MIDDLEWARE PARA RESTRICCION DE ROLES

// INVITADO
Route::group( ['middleware'=> ['guest']], function() {
    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::get('/login', 'Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login');
} );

// AUTENTICADOS
Route::group( ['middleware'=> ['auth']], function(){
    // NOTIFICACION
        Route::post( '/notification/get', 'NotificationController@get' );
    // DASHBOARD
        Route::get('/main', function () {
            return view('contenido.contenido');
        })->name('main');
        Route::get( '/dashboard', 'DashboardController' );
    // ALMACENERO
    Route::group( ['middleware'=> ['almacenero']], function(){
        // CATEGORIA
            Route::resource( '/categoria', 'CategoriaController' );
            Route::put( '/categoria/baja/id', 'CategoriaController@darBajaEstado' );
            Route::put( '/categoria/alta/id', 'CategoriaController@darAltaEstado' );
            Route::get( '/categorias', 'CategoriaController@listaCategorias' );
        // ARTICULO
            Route::resource( '/articulo', 'ArticuloController' );
            Route::put( '/articulo/baja/id', 'ArticuloController@darBajaEstado' );
            Route::put( '/articulo/alta/id', 'ArticuloController@darAltaEstado' );
            Route::get( '/articulo/buscar/filtro', 'ArticuloController@buscarArticulo' );
            Route::get( '/articulos', 'ArticuloController@getArticulos' );
            Route::get( '/articulos/stock', 'ArticuloController@buscarArticuloConStock' );
            Route::get( '/articulos/stock/venta', 'ArticuloController@buscarArticuloVenta' );
            Route::get( '/articulos/listar/pdf', 'ArticuloController@getArticulosPDF' );
        // PROVEEDOR - PERSONA
            Route::resource( '/proveedor', 'ProveedorController' );
            Route::get( '/proveedores', 'ProveedorController@getProveedores' );
        // INGRESO
            Route::resource('/ingreso', 'IngresoController');
            Route::put('/ingreso/baja/id', 'IngresoController@darBajaEstado');
            Route::get( '/ingreso/get/cabecera', 'IngresoController@getCabecera' );
            Route::get( '/ingreso/get/detalles',  'IngresoController@getDetalles' );
    } );
    // VENDEDOR
    Route::group( ['middleware'=> ['vendedor']], function(){
        // CLIENTE - PERSONA
            Route::resource( '/cliente', 'ClienteController' );
            Route::get( '/clientes', 'ClienteController@getClientes' );
        // VENTAS
            Route::resource('/venta', 'VentaController');
            Route::put('/venta/baja/id', 'VentaController@darBajaEstado');
            Route::get( '/venta/get/cabecera', 'VentaController@getCabecera' );
            Route::get( '/venta/get/detalles',  'VentaController@getDetalles' );
            Route::get('/venta/pdf/{id}', 'VentaController@getVentaPDF');
    } );
    // ADMIN
    Route::group( ['middleware'=> ['administrador']], function(){
        // CATEGORIA
            Route::resource( '/categoria', 'CategoriaController' );
            Route::put( '/categoria/baja/id', 'CategoriaController@darBajaEstado' );
            Route::put( '/categoria/alta/id', 'CategoriaController@darAltaEstado' );
            Route::get( '/categorias', 'CategoriaController@listaCategorias' );
        // CLIENTE - PERSONA
            Route::resource( '/cliente', 'ClienteController' );
            Route::get( '/clientes', 'ClienteController@getClientes' );
        // ARTICULO
            Route::resource( '/articulo', 'ArticuloController' );
            Route::put( '/articulo/baja/id', 'ArticuloController@darBajaEstado' );
            Route::put( '/articulo/alta/id', 'ArticuloController@darAltaEstado' );
            Route::get( '/articulo/buscar/filtro', 'ArticuloController@buscarArticulo' );
            Route::get( '/articulos', 'ArticuloController@getArticulos' );
            Route::get( '/articulos/stock', 'ArticuloController@buscarArticuloConStock' );
            Route::get( '/articulos/stock/venta', 'ArticuloController@buscarArticuloVenta' );
            Route::get( '/articulos/listar/pdf', 'ArticuloController@getArticulosPDF' );
        // PROVEEDOR - PERSONA
            Route::resource( '/proveedor', 'ProveedorController' );
            Route::get( '/proveedores', 'ProveedorController@getProveedores' );
        // ROL
            Route::resource( '/rol', 'RolController' );
            Route::put( '/rol/baja/id', 'RolController@darBajaEstado' );
            Route::put( '/rol/alta/id', 'RolController@darAltaEstado' );        
        // USUARIOS
            Route::resource( '/user', 'UserController' );
            Route::put( '/user/baja/id', 'UserController@darBajaEstado' );
            Route::put( '/user/alta/id', 'UserController@darAltaEstado' );
            Route::get( '/roles', 'RolController@listaRoles' );
        // INGRESO
            Route::resource('/ingreso', 'IngresoController');
            Route::put('/ingreso/baja/id', 'IngresoController@darBajaEstado');
            Route::get( '/ingreso/get/cabecera', 'IngresoController@getCabecera' );
            Route::get( '/ingreso/get/detalles',  'IngresoController@getDetalles' );
        // VENTAS
            Route::resource('/venta', 'VentaController');
            Route::put('/venta/baja/id', 'VentaController@darBajaEstado');
            Route::get( '/venta/get/cabecera', 'VentaController@getCabecera' );
            Route::get( '/venta/get/detalles',  'VentaController@getDetalles' );
            Route::get('/venta/pdf/{id}', 'VentaController@getVentaPDF');
    } );
    // LOGOUT
    Route::post('/logout', 'Auth\LoginController@logout');
} );

//Route::get('/', 'Auth\LoginController@showLoginForm');
//Route::get('/', 'Auth\LoginController@showLoginForm');
//Route::post('/login', 'Auth\LoginController@login');
//Route::get('/home', 'HomeController@index');
//Route::get('/home', 'HomeController@index');
    //->name('home');

//Route::resource('/ingreso', 'IngresoController');


/*Route::get('/pdf', function() {
    $articulos = Articulo::join('categorias', 'articulos.id_categoria', '=', 'categorias.id')
        ->select(   'articulos.id', 'articulos.codigo', 'articulos.nombre', 
                    'categorias.nombre as nombre_categoria', 'articulos.id_categoria',
                    'articulos.precio_venta', 'articulos.stock', 'articulos.descripcion',
                    'articulos.estado')
        ->orderBy('articulos.nombre', 'desc')->get(); 
    $cantidad = Articulo::count();
    return view( 'pdf.articulos_pdf', compact('articulos', 'cantidad') );
});
Route::get('/pdf/venta', 'VentaController@getVentaPDF');*/
