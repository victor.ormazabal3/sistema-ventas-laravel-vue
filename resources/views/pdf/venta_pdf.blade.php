 <!DOCTYPE html>
 <html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte de venta</title>
    <style>
        body {
        /*position: relative;*/
        /*width: 16cm;  */
        /*height: 29.7cm; */
        /*margin: 0 auto; */
        /*color: #555555;*/
        /*background: #FFFFFF; */
        font-family: Arial, sans-serif; 
        font-size: 14px;
        /*font-family: SourceSansPro;*/
        }

        #logo{
        float: left;
        margin-top: 1%;
        margin-left: 2%;
        margin-right: 2%;
        }

        #imagen{
        width: 100px;
        }

        #datos{
        float: left;
        margin-top: 0%;
        margin-left: 2%;
        margin-right: 2%;
        /*text-align: justify;*/
        }

        #encabezado{
        text-align: center;
        margin-left: 10%;
        margin-right: 35%;
        font-size: 15px;
        }

        #fact{
        /*position: relative;*/
        float: right;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
        font-size: 20px;
        }

        section{
        clear: left;
        }

        #cliente{
        text-align: left;
        }

        #facliente{
        width: 40%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #fac, #fv, #fa{
        color: #FFFFFF;
        font-size: 15px;
        }

        #facliente thead{
        padding: 20px;
        background: #2183E3;
        text-align: left;
        border-bottom: 1px solid #FFFFFF;  
        }

        #facvendedor{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #facvendedor thead{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #facarticulo{
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 15px;
        }

        #facarticulo thead{
        padding: 20px;
        background: #2183E3;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;  
        }

        #gracias{
        text-align: center; 
        }
    </style>
 </head>
 <body>
     @foreach ($venta as $item)
        <header>
            <div id="logo">
            <img src="img/logo.png" alt="Logo" id="imagen">
            </div>
            <div id="datos">
            <p id="encabezado">
                <b>Instituto Tecnologico de Chilpancingo</b>
                <br>
                <b>Ricardo Flores</b>
            </p>
            </div>
            <div id="fact">
                <p>{{$item->tipo_comprobante}} <br>  {{ $item->serie_comprobante }} - {{ $item->numero_comprobante }} </p>
            </div>
        </header>
        <br>
        <section>
            <div>
                <table id="facliente">
                    <thead>
                        <tr>
                            <td id="fac">Cliente</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>
                                <p id="cliente">
                                    Sr(a). {{ $item->nombre }} <br>
                                    Documento: {{ $item->tipo_documento }} <br>
                                    Numero documento: {{ $item->numero_documento }} <br>
                                    Direccion: {{ $item->direccion }} <br>
                                    Telefono: {{ $item->telefono }} <br>
                                    Email: {{ $item->email }}
                                </p>
                            </th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <br>
        <section>
            <div>
                <table id="facvendedor">
                    <thead>
                        <tr id="fv">
                            <th>Vendedor</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> {{ $item->usuario }} </td>
                            <td> {{ $item->fecha_hora }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    @endforeach
        <br>
    <section>
        <div>
            <table id="facarticulo">
                <thead>
                    <tr id="fa">
                        <th>Articulo</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Descuento</th>
                        <th>Precio total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detalles as $item)
                        <tr>    
                            <td> {{ $item->nombre }} </td>
                            <td> {{ $item->cantidad }} </td>
                            <td> {{ $item->precio }} </td>
                            <td> {{ $item->descuento }} </td>
                            <td> {{ $item->cantidad * $item->precio - $item->descuento }} </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    @foreach ($venta as $item)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Subtotal</td>
                        <td> {{ $item->total - $item->impuesto }} </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Impuesto</td>
                        <td> {{ round( $item->total * $item->impuesto, 2 ) }} </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Total</td>
                        <td> {{ $item->total }} </td>
                    </tr>
                    @endforeach
                </tfoot>
            </table>
        </div>
    </section>    
    <br>
    <br>
    <footer>
        <div id="gracias">
            <p><b>Gracias por su compra!</b></p>
        </div>
    </footer>
 </body>
 </html>