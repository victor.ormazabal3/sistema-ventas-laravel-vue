<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model {
    /*protected $table = 'articulos';
    protected $primaryKey = 'id';*/
    protected $fillable = [
        'id_categoria',
        'codigo',
        'nombre',
        'precio_venta',
        'stock',
        'descripcion',
        'estado'
    ];

    public function categoria() {
        return $this->belongsTo('App\Categoria');
    }
}
