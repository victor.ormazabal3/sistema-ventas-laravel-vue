<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Persona;

class ClienteController extends Controller {
    public function index(Request $request) {
        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') 
            $personas = Persona::orderBy('id', 'desc') ->paginate(3);
        else{
            $personas = Persona::where($criterio, 'like', '%' . $buscar . '%')
                ->orderBy('id', 'desc')
                ->paginate(3);
        }
        return [
            'paginacion' => [
                'total' => $personas->total(),
                'pagina_actual' => $personas->currentPage(),
                'por_pagina' => $personas->perPage(),
                'ultima_pagina' => $personas->lastPage(),
                'desde' => $personas->firstItem(),
                'hasta' => $personas->lastItem()
            ],
            'personas' => $personas
        ];
    }
    public function create() {
        
    }
    public function store(Request $request) {
        $persona = new Persona();
        $persona->nombre = $request->nombre;
        $persona->tipo_documento = $request->tipo_documento;
        $persona->numero_documento = $request->numero_documento;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;
        $persona->save();
    }
    public function show($id) {
        
    }
    public function edit($id) {
        
    }
    public function update(Request $request) {
        $persona = Persona::findOrFail( $request->id );
        $persona->nombre = $request->nombre;
        $persona->tipo_documento = $request->tipo_documento;
        $persona->numero_documento = $request->numero_documento;
        $persona->email = $request->email;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->save();
    }
    public function destroy($id) {
        
    }

    public function getClientes(Request $request) {
        //if(!$request->ajax()) return redirect( '/' );
        $filtro = $request->filtro;
        $clientes = Persona::where('nombre', 'like', '%'.$filtro.'%' )
            ->orWhere('numero_documento', 'like', '%'.$filtro.'%' )
            ->select('id','nombre','numero_documento')
            ->orderBy('nombre', 'asc')->get();
        return [ 'clientes' => $clientes ];
    }
}
