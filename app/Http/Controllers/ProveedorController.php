<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Proveedor;
use App\Persona;

class ProveedorController extends Controller {
    
    public function index(Request $request) {

        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $proveedores = Proveedor::join('personas', 'proveedores.id_persona', '=', 'personas.id')
                ->select(   'personas.id', 'personas.nombre', 'personas.tipo_documento',
                            'personas.numero_documento', 'personas.email',
                            'personas.direccion', 'personas.telefono', 'proveedores.contacto',
                            'proveedores.telefono_contacto' )
                ->orderBy('personas.id', 'desc')->paginate(1);
        }
        else{
            if($criterio == 'contacto'){    // para que busque en la tabla proveedores
                $proveedores = Proveedor::join('personas', 'proveedores.id_persona', '=', 'personas.id')
                ->select(   'personas.id', 'personas.nombre', 'personas.tipo_documento',
                            'personas.numero_documento', 'personas.email',
                            'personas.direccion', 'personas.telefono', 'proveedores.contacto',
                            'proveedores.telefono_contacto' )
                ->where('proveedores.contacto', 'like', '%' . $buscar . '%')
                ->orderBy('personas.id', 'desc')->paginate(1);
            }
            else{
                $proveedores = Proveedor::join('personas', 'proveedores.id_persona', '=', 'personas.id')
                ->select(   'personas.id', 'personas.nombre', 'personas.tipo_documento',
                            'personas.numero_documento', 'personas.email',
                            'personas.direccion', 'personas.telefono', 'proveedores.contacto',
                            'proveedores.telefono_contacto' )
                ->where('personas.' . $criterio, 'like', '%' . $buscar . '%')
                ->orderBy('personas.id', 'desc')->paginate(1);
            }
        }
        return [
            'paginacion' => [
                'total' => $proveedores->total(),
                'pagina_actual' => $proveedores->currentPage(),
                'por_pagina' => $proveedores->perPage(),
                'ultima_pagina' => $proveedores->lastPage(),
                'desde' => $proveedores->firstItem(),
                'hasta' => $proveedores->lastItem()
            ],
            'proveedores' => $proveedores
        ];
    }

    public function create() {
    
    }

    public function store(Request $request) {
        try{
            DB::beginTransaction();
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->numero_documento = $request->numero_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->save();    
            $proveedor = new Proveedor();
            $proveedor->id_persona = $persona->id;
            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->save();
            DB::commit();
        }
        catch(Exception $e){
            DB::rollBack();
        }
    }

    public function show($id) {
        
    }

    public function edit($id) {
        
    }

    public function update(Request $request) {
        try{
            DB::beginTransaction();
            $persona = Persona::findOrFail($request->id);
            $proveedor = Proveedor::findOrFail($request->id);
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->numero_documento = $request->numero_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->save();    
            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->save();
            DB::commit();
        }
        catch(Exception $e){
            DB::rollBack();
        }    
    }

    public function destroy($id) {
        
    }

    public function getProveedores(Request $request) {
        //if(!$request->ajax()) return redirect( '/' );
        $filtro = $request->filtro;
        $proveedores = Proveedor::join('personas', 'proveedores.id_persona', '=', 'personas.id')
            ->where('personas.nombre', 'like', '%'.$filtro.'%' )
            ->orWhere('personas.numero_documento', 'like', '%'.$filtro.'%' )
            ->select('personas.id','personas.nombre','personas.numero_documento','proveedores.contacto')
            ->orderBy('personas.nombre', 'asc')->get();
        return [ 'proveedores' => $proveedores ];
    }

}
