<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Ingreso;
use App\User;
use App\DetalleIngreso;
use Carbon\Carbon;
use App\Notifications\NotifyAdmin;

class IngresoController extends Controller {
 
    public function index(Request $request) {

        //if(!$request->ajax()) return redirect( '/' );

        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $ingresos = Ingreso::join('personas', 'ingresos.id_proveedor', '=', 'personas.id')
                ->join('users', 'ingresos.id_usuario', '=', 'users.id_persona')
                ->join('proveedores', 'ingresos.id_proveedor', '=', 'proveedores.id_persona')
                ->select(   'ingresos.id','ingresos.tipo_comprobante','ingresos.numero_comprobante',
                            'ingresos.serie_comprobante','ingresos.fecha_hora','ingresos.impuesto',
                            'ingresos.total','ingresos.estado',
                            'personas.id as proveedor_id', 'personas.nombre', 'proveedores.contacto', 'users.usuario' )
                ->orderBy('ingresos.id', 'desc')->paginate(3);
        }
        else{
            $ingresos = Ingreso::join('personas', 'ingresos.id_proveedor', '=', 'personas.id')
                ->join('users', 'ingresos.id_usuario', '=', 'users.id_persona')
                ->join('proveedores', 'ingresos.id_proveedor', '=', 'proveedores.id_persona')
                ->select(   'ingresos.id','ingresos.tipo_comprobante','ingresos.numero_comprobante',
                            'ingresos.serie_comprobante','ingresos.fecha_hora','ingresos.impuesto',
                            'ingresos.total','ingresos.estado',
                            'personas.id as proveedor_id', 'personas.nombre', 'proveedores.contacto', 'users.usuario' )
                ->where('ingresos.' . $criterio, 'like', '%' . $buscar . '%')
                ->orderBy('ingresos.id', 'desc')->paginate(3);
        }
        return [
            'paginacion' => [
                'total' => $ingresos->total(),
                'pagina_actual' => $ingresos->currentPage(),
                'por_pagina' => $ingresos->perPage(),
                'ultima_pagina' => $ingresos->lastPage(),
                'desde' => $ingresos->firstItem(),
                'hasta' => $ingresos->lastItem()
            ],
            'ingresos' => $ingresos
        ];
    }

    public function create() {}

    public function store(Request $request) {
        try{
            DB::beginTransaction();
            $time = Carbon::now('America/Mexico_City');
            $ingreso = new Ingreso();
            $ingreso->id_proveedor = $request->id_proveedor;
            $ingreso->id_usuario = \Auth::user()->id_persona;
            $ingreso->tipo_comprobante = $request->tipo_comprobante;
            $ingreso->serie_comprobante = $request->serie_comprobante;
            $ingreso->numero_comprobante = $request->numero_comprobante;
            $ingreso->fecha_hora = $time->toDateTimeString();
            $ingreso->impuesto = $request->impuesto;
            $ingreso->total = $request->total;
            $ingreso->estado = 'Registrado';
            $ingreso->save();

            $detalles = $request->data;      // lista de detalles
            for($i=0; $i<count($detalles); $i++) {
                $detalle = new DetalleIngreso();
                $detalle->id_ingreso = $ingreso->id;
                $detalle->id_articulo = $detalles[$i]['id_articulo'];
                $detalle->cantidad = $detalles[$i]['cantidad'];
                $detalle->precio = $detalles[$i]['precio'];
                
                //$detalle->cantidad = $detalles[$i]->cantidad;
                //$detalle->precio = $detalles[$i]->precio;
                $detalle->save();
            }
            $fecha_actual = date( 'Y-m-d' );
            $numero_ventas = DB::table('ventas')->whereDate( 'created_at', $fecha_actual )->count();
            $numero_ingresos = DB::table('ingresos')->whereDate( 'created_at', $fecha_actual )->count();
            $datos = [
                'ventas' => [
                    'numero'=> $numero_ventas,
                    'msj'=> 'Ventas'
                ],
                'ingresos' => [
                    'numero'=> $numero_ingresos,
                    'msj'=> 'Ingresos'
                ],
            ];
            $all_users = User::all();
            foreach( $all_users as $user ) {
                User::findOrFail( $user->id_persona )->notify( new NotifyAdmin( $datos ) );
            }
            DB::commit();
        }
        catch(Exception $e){
            DB::rollBack();
        }
    }
    
    public function show($id) {
        
    }

    public function edit($id) {
        
    }

    public function update(Request $request, $id) {
        
    }
    
    public function destroy($id) {
        
    }

    public function darBajaEstado(Request $request) {
        $ingreso = Ingreso::findOrFail( $request->id );
        $ingreso->estado = 'Anulado';
        $ingreso->save();
    }

    public function getCabecera(Request $request){
        $id = $request->id;
        $cabecera = Ingreso::join('personas', 'ingresos.id_proveedor', '=', 'personas.id')
            ->join('users', 'ingresos.id_usuario', '=', 'users.id_persona')
            ->select(   'ingresos.id','ingresos.tipo_comprobante','ingresos.numero_comprobante',
                        'ingresos.serie_comprobante','ingresos.fecha_hora','ingresos.impuesto',
                        'ingresos.total','ingresos.estado', 'personas.nombre', 'users.usuario' )
            ->where('ingresos.id','=',$id)
            ->orderBy('ingresos.id', 'desc')->take(1)->get();
        return [ 'cabecera' => $cabecera ];
    }
    public function getDetalles(Request $request){
        $id = $request->id;
        $detalles = DetalleIngreso::join('articulos', 'detalle_ingresos.id_articulo', '=', 'articulos.id')
            ->select(   'detalle_ingresos.id', 'detalle_ingresos.cantidad','detalle_ingresos.precio',
            'articulos.id', 'articulos.nombre', 'articulos.codigo' )
            ->where('detalle_ingresos.id_ingreso','=',$id)
            ->orderBy('detalle_ingresos.id', 'desc')->get();
        return [ 'detalles' => $detalles ];
    }

}
