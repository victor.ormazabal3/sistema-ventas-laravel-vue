<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Persona;

class UserController extends Controller { 

    public function index(Request $request) {
        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $users = User::join('personas', 'users.id_persona', '=', 'personas.id')
                ->join('roles', 'users.id_rol', '=', 'roles.id')
                ->select(   'personas.id', 'personas.nombre', 'personas.tipo_documento',
                            'personas.numero_documento', 'personas.email',
                            'personas.direccion', 'personas.telefono', 'users.usuario',
                            'users.password', 'users.estado', 'roles.id as id_rol', 'roles.nombre as rol',
                            'roles.descripcion' )
                ->orderBy('personas.id', 'desc')->paginate(2);
        }
        else{
            if($criterio == 'usuario'){
                $users = User::join('personas', 'users.id_persona', '=', 'personas.id')
                    ->join('roles', 'users.id_rol', '=', 'roles.id')
                    ->select(   'personas.id', 'personas.nombre', 'personas.tipo_documento',
                                'personas.numero_documento', 'personas.email',
                                'personas.direccion', 'personas.telefono', 'users.usuario',
                                'users.password', 'users.estado', 'roles.id as id_rol', 'roles.nombre as rol',
                                'roles.descripcion' )
                    ->where('users.' . $criterio, 'like', '%' . $buscar . '%')
                    ->orderBy('personas.id', 'desc')->paginate(2);
            }
            else{
                $users = User::join('personas', 'users.id_persona', '=', 'personas.id')
                    ->join('roles', 'users.id_rol', '=', 'roles.id')
                    ->select(   'personas.id', 'personas.nombre', 'personas.tipo_documento',
                                'personas.numero_documento', 'personas.email',
                                'personas.direccion', 'personas.telefono', 'users.usuario',
                                'users.password', 'users.estado', 'roles.id as id_rol', 'roles.nombre as rol',
                                'roles.descripcion' )
                    ->where('personas.' . $criterio, 'like', '%' . $buscar . '%')
                    ->orderBy('personas.id', 'desc')->paginate(2);
            }
        }
        return [
            'paginacion' => [
                'total' => $users->total(),
                'pagina_actual' => $users->currentPage(),
                'por_pagina' => $users->perPage(),
                'ultima_pagina' => $users->lastPage(),
                'desde' => $users->firstItem(),
                'hasta' => $users->lastItem()
            ],
            'users' => $users
        ];
    }
    public function create() {
        
    }
    public function store(Request $request) {
        try{
            DB::beginTransaction();
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->numero_documento = $request->numero_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->save();    
            $user = new User();
            $user->id_persona = $persona->id;
            $user->usuario = $request->usuario;
            $user->password = bcrypt($request->password);
            $user->estado = '1';
            $user->id_rol = $request->id_rol;
            $user->save();
            DB::commit();
        }
        catch(Exception $e){
            DB::rollBack();
        }
    }
    public function show($id) {
        
    }
    public function edit($id) {
        
    }
    public function update(Request $request, $id) {
        try{
            DB::beginTransaction();
            $persona = Persona::findOrFail($request->id);
            $user = User::findOrFail($request->id);
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->numero_documento = $request->numero_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;
            $persona->save();    
            $user->usuario = $request->usuario;
            $user->password = bcrypt($request->password);
            $user->estado = '1';
            $user->id_rol = $request->id_rol;
            $user->save();
            DB::commit();
        }
        catch(Exception $e){
            DB::rollBack();
        }
    }
    public function destroy($id) {
        
    }
    public function darBajaEstado(Request $request) {
        $user = User::findOrFail( $request->id );
        $user->estado = '0';
        $user->save();
    }

    public function darAltaEstado(Request $request) {
        $user = User::findOrFail( $request->id );
        $user->estado = '1';
        $user->save();
    }
}
