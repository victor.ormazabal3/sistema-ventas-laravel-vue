<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model {

    protected $fillable = [
        'id_cliente',
        'id_usuario',
        'tipo_comprobante',
        'serie_comprobante',
        'numero_comprobante',
        'fecha_hora',
        'impuesto',
        'total',
        'estado',
    ];

    /*public function cliente() {
        return $this->belongsTo('App\Persona');
    }

    public function usuario() {
        return $this->belongsTo('App\User');
    }*/

}
